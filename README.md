# grub2-theme-vimix-git

[![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](http://www.gnu.org/licenses/gpl-3.0)

Grub2 theme Vimix

**v2021.09.19-1.1**

https://github.com/vinceliuice/grub2-themes/

How to clone this repo:

```
git clone https://gitlab.com/rebornos-team/rebornos-special-system-files/grub2/grub2-theme-vimix-git.git
```

PKGBUILD modified to add the following files:

theme.txt

unifont-regular-16.pf2

It is added at the end:

```
    mkdir -p ${pkgdir}/boot/grub/themes/${_theme}
    install -Dm644 ${srcdir}/theme.txt ${pkgdir}/boot/grub/themes/${_theme}
    install -Dm644 ${srcdir}/unifont-regular-16.pf2 ${pkgdir}/boot/grub/themes/${_theme}
}
```
